package com.example.demo.service;

import com.example.demo.CaseApplication;
import com.example.demo.model.request.review.ReviewGetRequest;
import com.example.demo.model.request.review.ReviewPostRequest;
import com.example.demo.model.response.review.ReviewGetResponse;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CaseApplication.class)
class ReviewServiceDatabaseIntegrationTest {

    @Autowired
    ReviewService reviewService;

    @Test
    void insertNewArticle_thenGetByTitleAndStarCount() {

        ReviewPostRequest postRequest = new ReviewPostRequest();
        String testReviewer = "John";
        String testArticleId = "1";
        postRequest.setReviewer(testReviewer);
        postRequest.setArticleId(testArticleId);

        reviewService.postReviewByRequest(postRequest);

        ReviewGetRequest getRequest = new ReviewGetRequest();
        getRequest.setReviewer(testReviewer);
        getRequest.setArticleId(testArticleId);

        ReviewGetResponse testResponse = reviewService.getReviewByRequest(getRequest);

        assertThat(testResponse.getReviewList().isEmpty()).isFalse();
        assertThat(
                testResponse.getReviewList().get(0).getReviewer().
                        equals(testReviewer)
        ).isTrue();
        assertThat(
                testResponse.getReviewList().get(0).getArticle().getId()
                        == Integer.parseInt(testArticleId)
        ).isSameAs(true);
        assertThat(
                testResponse.getReviewList().get(0).getReviewer().
                        equals(testReviewer)
        ).isTrue();
        assertThat(testResponse.getReviewList().get(0).getReviewContent()).isNull();
    }
}
