package com.example.demo.service;

import com.example.demo.entity.Review;
import com.example.demo.exception.AllParametersAreNullException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.request.review.ReviewDeleteRequest;
import com.example.demo.model.request.review.ReviewGetRequest;
import com.example.demo.model.request.review.ReviewPostRequest;
import com.example.demo.model.request.review.ReviewPutRequest;
import com.example.demo.model.response.review.ReviewDeleteResponse;
import com.example.demo.model.response.review.ReviewGetResponse;
import com.example.demo.repository.ArticleRepository;
import com.example.demo.repository.ReviewRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ReviewServiceUnitTest {

    @InjectMocks
    private ReviewService reviewService;

    @Mock
    private ArticleRepository articleRepository;

    @Mock
    private ReviewRepository reviewRepository;

    @Test
    void getReviewByValidId() {
        ReviewGetRequest reviewGetRequest = new ReviewGetRequest();

        String reviewId = "2";
        reviewGetRequest.setReviewId(reviewId);

        Review review = new Review();
        review.setId(Integer.parseInt(reviewId));
        Mockito.when(reviewRepository.getReviewListById(Integer.parseInt(reviewId))).thenReturn(Arrays.asList(review));

        ReviewGetResponse reviewGetResponse = reviewService.getReviewByRequest(reviewGetRequest);



        assertThat(reviewGetResponse.getReviewList(), equalTo(Arrays.asList(review)));
    }

    @Test
    void deleteReviewByValidId() {
        ReviewDeleteRequest reviewDeleteRequest = new ReviewDeleteRequest();

        String reviewId = "2";
        reviewDeleteRequest.setReviewId(reviewId);

        Review review = new Review();
        review.setId(Integer.parseInt(reviewId));

        Mockito.when(reviewRepository.getReviewListById(Integer.parseInt(reviewId))).thenReturn(Arrays.asList(review));
        Mockito.when(reviewRepository.deleteReviewById(Integer.parseInt(reviewId))).thenReturn(1L);

        ReviewDeleteResponse reviewDeleteResponse = reviewService.deleteReviewByRequest(reviewDeleteRequest);

        assertThat(reviewDeleteResponse.getReviewList(), equalTo(Arrays.asList(review)));
    }

    @Test
    void postReviewByRequest() {
        ReviewPostRequest reviewPostRequest = new ReviewPostRequest();

        String reviewId = null;
        reviewPostRequest.setReviewId(reviewId);

        String reviewer = null;
        reviewPostRequest.setReviewer(reviewer);

        String reviewContent = null;
        reviewPostRequest.setReviewContent(reviewContent);

        Mockito.lenient().doNothing().when(reviewRepository)
                .postReviewByParameters(null, null, null);

        Exception exception = assertThrows(AllParametersAreNullException.class, () -> {
            reviewService.postReviewByRequest(reviewPostRequest);
        });

        String expectedMessage = "Couldn't add a new review record since no parameter in provided!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void putReviewByRequest() {
        ReviewPutRequest reviewPutRequest = new ReviewPutRequest();
        String reviewId = "6";
        reviewPutRequest.setReviewId(reviewId);

        Mockito.when(reviewRepository.getReviewListById(Integer.parseInt(reviewId)))
                .thenReturn(new ArrayList<>());

        Exception exception = assertThrows(NotFoundException.class, () -> {
            reviewService.putReviewByRequest(reviewPutRequest);
        });

        String expectedMessage = "Review with id=" + reviewId + " is not found!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));


    }
}