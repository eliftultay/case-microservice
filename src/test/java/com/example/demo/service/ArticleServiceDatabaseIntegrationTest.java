package com.example.demo.service;

import com.example.demo.CaseApplication;
import com.example.demo.model.request.article.ArticleGetRequest;
import com.example.demo.model.request.article.ArticlePostRequest;
import com.example.demo.model.response.article.ArticleGetResponse;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CaseApplication.class)
class ArticleServiceDatabaseIntegrationTest {

    @Autowired
    ArticleService articleService;

    @Test
    void insertNewArticle_thenGetByTitleAndStarCount() {

        ArticlePostRequest postRequest = new ArticlePostRequest();
        String testArticleTitle = "John";
        String testStarCount = "6";
        postRequest.setTitle(testArticleTitle);
        postRequest.setStarCount(testStarCount);

        articleService.postArticleByRequest(postRequest);

        ArticleGetRequest getRequest = new ArticleGetRequest();
        getRequest.setTitle(testArticleTitle);
        getRequest.setStarCount(testStarCount);

        ArticleGetResponse testResponse = articleService.getArticleByRequest(getRequest);

        assertThat(testResponse.getArticleList().isEmpty()).isFalse();
        assertThat(
                testResponse.getArticleList().get(0).getTitle().
                        equals(testArticleTitle)
        ).isTrue();
        assertThat(
                testResponse.getArticleList().get(0).getStarCount()
                        == Integer.parseInt(testStarCount)
        ).isTrue();
        assertThat(
                testResponse.getArticleList().get(0).getTitle().
                        equals(testArticleTitle)
        ).isTrue();
        assertThat(testResponse.getArticleList().get(0).getAuthor()).isNull();
        assertThat(testResponse.getArticleList().get(0).getArticleContent()).isNull();
        assertThat(testResponse.getArticleList().get(0).getPublishDate()).isNull();
    }
}
