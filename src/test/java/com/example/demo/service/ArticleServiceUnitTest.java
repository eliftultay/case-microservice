package com.example.demo.service;

import com.example.demo.entity.Article;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.request.article.ArticleDeleteRequest;
import com.example.demo.model.request.article.ArticleGetRequest;
import com.example.demo.model.request.article.ArticlePostRequest;
import com.example.demo.model.request.article.ArticlePutRequest;
import com.example.demo.model.response.article.ArticleDeleteResponse;
import com.example.demo.model.response.article.ArticleGetResponse;
import com.example.demo.model.response.article.ArticlePostResponse;
import com.example.demo.repository.ArticleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class ArticleServiceUnitTest {

    @InjectMocks
    ArticleService articleService;

    @Mock
    private ArticleRepository articleRepository;

    @Test
    public void getArticleByValidId() {
        ArticleGetRequest articleGetRequest = new ArticleGetRequest();
        String testArticleId = "2";
        articleGetRequest.setArticleId(testArticleId);

        Article article = new Article();
        article.setId(2);
        Mockito.when(articleRepository.getArticleListById(2)).thenReturn(Arrays.asList(article));

        ArticleGetResponse articleGetResponse = articleService.getArticleByRequest(articleGetRequest);

        assertThat(articleGetResponse.getArticleList(), equalTo(Arrays.asList(article)));
    }

    @Test
    public void deleteArticleByValidId() {
        ArticleDeleteRequest articleDeleteRequest = new ArticleDeleteRequest();
        String testArticleId = "3";
        articleDeleteRequest.setArticleId(testArticleId);

        Article article = new Article();
        article.setId(Integer.parseInt(testArticleId));

        Mockito.when(articleRepository.getArticleListById(3)).thenReturn(Arrays.asList(article));
        Mockito.doNothing().when(articleRepository).deleteArticleById(Integer.parseInt(testArticleId));

        ArticleDeleteResponse articleDeleteResponse = articleService.deleteArticleByRequest(articleDeleteRequest);

        assertThat(articleDeleteResponse.getArticleList(), equalTo(Arrays.asList(article)));
    }

    @Test
    public void postArticleWithAllParameters() {
        String title = "Title Unit Test";
        String author = "Author Unit Test";
        String articleContent = "Content Unit Test";
        LocalDate publishDate = null;
        String starCount = "8";

        ArticlePostRequest articlePostRequest = new ArticlePostRequest();
        articlePostRequest.setTitle(title);
        articlePostRequest.setAuthor(author);
        articlePostRequest.setArticleContent(articleContent);
        articlePostRequest.setPublishDate(publishDate);
        articlePostRequest.setStarCount(starCount);

        Article article = new Article();
        article.setTitle(title);
        article.setAuthor(author);
        article.setArticleContent(articleContent);
        article.setPublishDate(publishDate);
        article.setStarCount(Integer.parseInt(starCount));

        Mockito.doNothing().when(articleRepository)
                .postArticleByParameters(title, author, articleContent, publishDate, starCount);

        Mockito.when(articleRepository.getArticleListByParameters(title, author, articleContent, publishDate, starCount))
                .thenReturn(Arrays.asList(article));

        ArticlePostResponse articlePostResponse = articleService.postArticleByRequest(articlePostRequest);

        assertThat(articlePostResponse.getArticleList(), equalTo(Arrays.asList(article)));
    }

    @Test()
    public void putArticleByRequest() {
        ArticlePutRequest articlePutRequest = new ArticlePutRequest();
        String articleId = "38";
        articlePutRequest.setArticleId(articleId);

        Mockito.when(articleRepository.getArticleListById(Integer.parseInt(articleId)))
                .thenReturn(new ArrayList<>());

        Exception exception = assertThrows(NotFoundException.class, () -> {
            articleService.putArticleByRequest(articlePutRequest);
        });

        String expectedMessage = "Article with id=" + articleId + " is not found!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
}