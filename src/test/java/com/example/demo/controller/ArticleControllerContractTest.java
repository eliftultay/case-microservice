package com.example.demo.controller;

import com.example.demo.CaseApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CaseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ArticleControllerContractTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @WithMockUser("root")
    @Test
    public void getArticle() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/articles")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.articleList").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.articleList[*].id").isNotEmpty());
    }

    @WithMockUser("root")
    @Test
    public void getArticleById() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/articles?articleId=1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.articleList[*].id").value(1));
    }

    @WithMockUser("root")
    @Test
    public void deleteArticleById() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .delete("/articles?articleId=1")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.articleList[*].id").value(1));
    }

    @WithMockUser("root")
    @Test
    public void postArticleByParameters() throws Exception {
        //TODO check post test method is inserting correct or not
        mvc.perform(MockMvcRequestBuilders
                .post("/articles?title=Howtopostarticle&author=xxx&&starCount=8")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
    }

    @WithMockUser("root")
    @Test
    public void putArticle() throws Exception {
        //TODO check put test method is updating correct or not
        mvc.perform(MockMvcRequestBuilders
                .put("/articles?articleId=2&title=Howtopostarticle&author=isabel&starCount=8")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.articleList[*].id").value(2));
    }
}