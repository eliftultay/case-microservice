INSERT INTO caseproject.article
(id, article_content, author, publish_date, star_count, title)
VALUES
(1, 'SampleArticleContent1', 'author1', DATE '2001-01-11', 4, 'title1'),
(2, 'SampleArticleContent2', 'author2', DATE '2002-02-02', 3, 'title2'),
(3, 'SampleArticleContent3', 'author3', DATE '2003-03-23', 3, 'title3');

INSERT INTO caseproject.review
(review_content, reviewer, article_id)
VALUES
('SampleReviewContent1', 'reviewer1', 1),
('SampleReviewContent2', 'reviewer2', 2),
('SampleReviewContent3', 'reviewer3', 2),
('SampleReviewContent4', 'reviewer1', 3),
('SampleReviewContent5', 'reviewer2', 3),
('SampleReviewContent6','reviewer3', 3);