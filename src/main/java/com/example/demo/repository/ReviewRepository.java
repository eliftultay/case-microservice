package com.example.demo.repository;

import com.example.demo.entity.Article;
import com.example.demo.entity.QReview;
import com.example.demo.entity.Review;
import com.querydsl.jpa.JPAQueryBase;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.List;

@Repository
public class ReviewRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    DataSource dataSource;

    public List<Review> getReviewListById(int id) {

        QReview review = QReview.review;
        JPAQuery query = new JPAQuery(entityManager);
        JPAQueryBase base = query.from(review);

        base = (JPAQueryBase) base.where(review.id.eq(id));

        return base.fetch();

    }

    public List<Review> getReviewListByParameters(
            String articleId, String reviewer, String reviewContent) {
        QReview review = QReview.review;
        JPAQuery query = new JPAQuery(entityManager);
        JPAQueryBase base = query.from(review);

        if (articleId == null && reviewer == null && reviewContent == null) {
            return base.fetch();
        }

        if (articleId != null) {
            base = (JPAQueryBase) base.where(review.article.id.eq(Integer.valueOf(articleId)));
        }

        if (reviewer != null) {
            base = (JPAQueryBase) base.where(review.reviewer.eq(reviewer));
        }

        if (reviewContent != null) {
            base = (JPAQueryBase) base.where(review.reviewContent.eq(reviewContent));
        }
        return base.fetch();
    }

    public long deleteReviewById(int id) {
        QReview review = QReview.review;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        JPADeleteClause base = queryFactory.delete(review);

        base = base.where(review.id.eq(id));

        return base.execute();
    }

    public void deleteReviewListByParameters(String articleId, String reviewer, String reviewContent) {
        QReview review = QReview.review;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        JPADeleteClause base = queryFactory.delete(review);

        if (articleId == null && reviewer == null && reviewContent == null) {
            base.execute();
        }

        if (articleId != null) {
            base = base.where(review.article.id.eq(Integer.valueOf(articleId)));
        }

        if (reviewer != null) {
            base = base.where(review.reviewer.eq(reviewer));
        }

        if (reviewContent != null) {
            base = base.where(review.reviewContent.eq(reviewContent));
        }

        base.execute();
    }

    public void postReviewByParameters(Article article, String reviewer, String reviewContent) {
        Review review = new Review();
        review.setArticle(article);
        review.setReviewer(reviewer);
        review.setReviewContent(reviewContent);
        entityManager.persist(review);
    }

    public long updateReviewById(int id, Article article, String reviewer, String reviewContent) {
        QReview review = QReview.review;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        JPAUpdateClause base = queryFactory.update(review).where(review.id.eq(id));

        if (article == null && reviewer == null && reviewContent == null) {
            return 0;
        }

        if (article != null) {
            base = base.set(review.article, article);
        }

        if (reviewContent != null) {
            base = base.set(review.reviewContent, reviewContent);
        }

        if (reviewer != null) {
            base = base.set(review.reviewer, reviewer);
        }

        entityManager.clear();

        return base.execute();
    }
}
