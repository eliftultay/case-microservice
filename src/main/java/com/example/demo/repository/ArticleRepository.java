package com.example.demo.repository;

import com.example.demo.entity.Article;
import com.example.demo.entity.QArticle;
import com.example.demo.entity.QReview;
import com.querydsl.jpa.JPAQueryBase;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;

@Repository
public class ArticleRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    DataSource dataSource;

    public List<Article> getArticleListByParameters(String title,
                                                    String author,
                                                    String articleContent,
                                                    LocalDate publishDate,
                                                    String starCount) {
        JPAQuery query = new JPAQuery(entityManager);
        QArticle article = QArticle.article;

        JPAQueryBase base = query.from(article);

        List<Article> articleList;

        if (title == null && author == null && articleContent == null
                && publishDate == null && starCount == null) {
            //Select all from article
            return base.fetch();
        }

        if (title != null) {
            base = (JPAQueryBase) base.where(article.title.eq(title));
        }

        if (author != null) {
            base = (JPAQueryBase) base.where(article.author.eq(author));
        }

        if (articleContent != null) {
            base = (JPAQueryBase) base.where(article.articleContent.eq(articleContent));
        }

        if (publishDate != null) {
            base = (JPAQueryBase) base.where(article.publishDate.eq(publishDate));
        }

        if (starCount != null) {
            base = (JPAQueryBase) base.where(article.starCount.eq(Integer.valueOf(starCount)));
        }

        return base.fetch();
    }

    public List<Article> getArticleListById(int id) {
        QArticle article = QArticle.article;
        JPAQuery<Article> query = new JPAQuery<Article>(entityManager);

        List<Article> articleList = query.from(article).where(article.id.eq(id)).fetch();

        return articleList;
    }

    public void deleteArticleListByParameters(String title,
                                              String author,
                                              String articleContent,
                                              LocalDate publishDate,
                                              String starCount) {

        QArticle article = QArticle.article;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        JPADeleteClause base = queryFactory.delete(article);

        if (title == null && author == null && articleContent == null
                && publishDate == null && starCount == null) {
            base.execute();
        }

        if (title != null) {
            base = base.where(article.title.eq(title));
        }

        if (author != null) {
            base = base.where(article.author.eq(author));
        }

        if (articleContent != null) {
            base = base.where(article.articleContent.eq(articleContent));
        }

        if (publishDate != null) {
            base = base.where(article.publishDate.eq(publishDate));
        }

        if (starCount != null) {
            base = base.where(article.starCount.eq(Integer.valueOf(starCount)));
        }

        base.execute();
    }

    public void deleteArticleById(int id) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        QReview review = QReview.review;
        JPADeleteClause reviewDeleteBase = queryFactory.delete(review);

        reviewDeleteBase = reviewDeleteBase.where(review.article.id.eq(id));
        reviewDeleteBase.execute();

        QArticle article = QArticle.article;
        JPADeleteClause base = queryFactory.delete(article);

        base = base.where(article.id.eq(id));
        base.execute();
    }

    public void postArticleByParameters(String title,
                                        String author,
                                        String articleContent,
                                        LocalDate publishDate,
                                        String starCount) {

        Article article = new Article();
        article.setTitle(title);
        article.setAuthor(author);
        article.setArticleContent(articleContent);
        article.setPublishDate(publishDate);
        if (starCount != null) {
            article.setStarCount(Integer.parseInt(starCount));
        }
        entityManager.persist(article);
    }

    public long updateArticleById(int id, String title,
                                  String author,
                                  String articleContent,
                                  LocalDate publishDate,
                                  String starCount) {
        QArticle article = QArticle.article;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        JPAUpdateClause base = queryFactory.update(article).where(article.id.eq(id));

        if (title == null && author == null && articleContent == null
                && publishDate == null && starCount == null) {
            return 0;
        }

        if (title != null) {
            base = base.set(article.title, title);
        }

        if (author != null) {
            base = base.set(article.author, author);
        }

        if (articleContent != null) {
            base = base.set(article.articleContent, articleContent);
        }

        if (publishDate != null) {
            base = base.set(article.publishDate, publishDate);
        }

        if (starCount != null) {
            base = base.set(article.starCount, Integer.parseInt(starCount));
        }
        long affectedRowCount = base.execute();

        entityManager.flush();

        return affectedRowCount;
    }
}
