package com.example.demo.model.request.review;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReviewBaseRequest {
    String articleId;
    String reviewer;
    String reviewContent;
}
