package com.example.demo.model.request.article;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/*
* To Reduce code repetition, an abstract class containing common fields.
* By defining it as an abstract class, creation of an object is prevented.
* */

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class ArticleBaseRequest {
    String title;
    String author;
    String articleContent;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    LocalDate publishDate;
    String starCount;
}
