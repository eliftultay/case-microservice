package com.example.demo.model.request.article;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
/*
* Class extends ArticleBaseRequest.
* Specific field created.
* */
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ArticleGetRequest extends ArticleBaseRequest {
    String articleId;
}
