package com.example.demo.model.request.review;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReviewPutRequest extends ReviewBaseRequest {
    @NotNull(message = "Review ID must not be null while updating!")
    String reviewId;
}
