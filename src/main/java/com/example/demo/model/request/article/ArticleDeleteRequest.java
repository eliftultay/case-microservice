package com.example.demo.model.request.article;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ArticleDeleteRequest extends ArticleBaseRequest {
    String articleId;
}
