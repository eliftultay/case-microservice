package com.example.demo.model.request.article;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

/*
 * Specifically defined(NotNull).
 * */

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ArticlePutRequest extends ArticleBaseRequest {
    @NotNull(message = "Article ID must not be null while updating!")
    String articleId;
}
