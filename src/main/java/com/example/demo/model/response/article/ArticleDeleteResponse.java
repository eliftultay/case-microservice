package com.example.demo.model.response.article;

import com.example.demo.entity.Article;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ArticleDeleteResponse {
    List<Article> articleList;
}
