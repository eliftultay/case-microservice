package com.example.demo.controller;

import com.example.demo.model.request.review.*;
import com.example.demo.model.response.review.ReviewDeleteResponse;
import com.example.demo.model.response.review.ReviewGetResponse;
import com.example.demo.model.response.review.ReviewPostResponse;
import com.example.demo.model.response.review.ReviewPutResponse;
import com.example.demo.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/reviews")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<ReviewGetResponse> getReview(ReviewGetRequest reviewGetRequest) {
        return new ResponseEntity<>(reviewService.getReviewByRequest(reviewGetRequest), HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseBody
    public ResponseEntity<ReviewDeleteResponse> deleteReview(ReviewDeleteRequest reviewDeleteRequest) {
        return new ResponseEntity<>(reviewService.deleteReviewByRequest(reviewDeleteRequest), HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<ReviewPostResponse> postReview(ReviewPostRequest reviewPostRequest) {
        return new ResponseEntity<>(reviewService.postReviewByRequest(reviewPostRequest), HttpStatus.CREATED);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<ReviewPutResponse> putReview(ReviewPutRequest reviewPutRequest) {
        return new ResponseEntity<>(reviewService.putReviewByRequest(reviewPutRequest), HttpStatus.CREATED);
    }

}
