package com.example.demo.controller;

import com.example.demo.model.request.article.ArticleDeleteRequest;
import com.example.demo.model.request.article.ArticleGetRequest;
import com.example.demo.model.request.article.ArticlePostRequest;
import com.example.demo.model.request.article.ArticlePutRequest;
import com.example.demo.model.response.article.ArticleDeleteResponse;
import com.example.demo.model.response.article.ArticleGetResponse;
import com.example.demo.model.response.article.ArticlePostResponse;
import com.example.demo.model.response.article.ArticlePutResponse;
import com.example.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/articles")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping
    @ResponseBody
    public ResponseEntity<ArticleGetResponse> getArticle(ArticleGetRequest articleGetRequest) {
        return new ResponseEntity<>(articleService.getArticleByRequest(articleGetRequest), HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseBody
    public ResponseEntity<ArticleDeleteResponse> deleteArticle(ArticleDeleteRequest articleDeleteRequest) {
        return new ResponseEntity<>(articleService.deleteArticleByRequest(articleDeleteRequest), HttpStatus.OK);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<ArticlePostResponse> postArticle(ArticlePostRequest articlePostRequest) {
        return new ResponseEntity<>(articleService.postArticleByRequest(articlePostRequest), HttpStatus.CREATED);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<ArticlePutResponse> putArticle(ArticlePutRequest articlePutRequest) {
        return new ResponseEntity<>(articleService.putArticleByRequest(articlePutRequest), HttpStatus.CREATED);
    }
}