package com.example.demo.service;

import com.example.demo.entity.Article;
import com.example.demo.exception.AllParametersAreNullException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.request.article.ArticleDeleteRequest;
import com.example.demo.model.request.article.ArticleGetRequest;
import com.example.demo.model.request.article.ArticlePostRequest;
import com.example.demo.model.request.article.ArticlePutRequest;
import com.example.demo.model.response.article.ArticleDeleteResponse;
import com.example.demo.model.response.article.ArticleGetResponse;
import com.example.demo.model.response.article.ArticlePostResponse;
import com.example.demo.model.response.article.ArticlePutResponse;
import com.example.demo.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

/*
* Transaction: A sequence of DB operations that  have to be executed all together or
* MUST NOT be executed at all.
* Transactional: Annotation that makes a method get executed in a transaction.
* */

@Service
@Transactional
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public ArticleGetResponse getArticleByRequest(ArticleGetRequest articleGetRequest) {
        List<Article> articleList;

        if (articleGetRequest.getArticleId() != null) {
            int id = Integer.parseInt(articleGetRequest.getArticleId());
            articleList = articleRepository.getArticleListById(id);
        } else {
            String title = articleGetRequest.getTitle();
            String author = articleGetRequest.getAuthor();
            String articleContent = articleGetRequest.getArticleContent();
            LocalDate publishDate = articleGetRequest.getPublishDate();
            String starCount = articleGetRequest.getStarCount();
            articleList = articleRepository.getArticleListByParameters(title, author,
                    articleContent, publishDate, starCount);
        }

        return new ArticleGetResponse(articleList);
    }

    public ArticleDeleteResponse deleteArticleByRequest(ArticleDeleteRequest articleDeleteRequest) {
        List<Article> articleList;

        if (articleDeleteRequest.getArticleId() != null) {
            int id = Integer.parseInt(articleDeleteRequest.getArticleId());
            articleList = articleRepository.getArticleListById(id);
            articleRepository.deleteArticleById(id);
        } else {
            String title = articleDeleteRequest.getTitle();
            String author = articleDeleteRequest.getAuthor();
            String articleContent = articleDeleteRequest.getArticleContent();
            LocalDate publishDate = articleDeleteRequest.getPublishDate();
            String starCount = articleDeleteRequest.getStarCount();
            if (title == null && author == null && articleContent == null
                    && publishDate == null && starCount == null) {
                throw new AllParametersAreNullException("Couldn't delete the article record since no parameter in provided!");
            } else {
                articleList = articleRepository.getArticleListByParameters(title, author,
                        articleContent, publishDate, starCount);
                articleList.forEach(article -> articleRepository.deleteArticleById(article.getId()));
            }
        }
        return ArticleDeleteResponse.builder().articleList(articleList).build();
    }

    public ArticlePostResponse postArticleByRequest(ArticlePostRequest articlePostRequest) {
        String title = articlePostRequest.getTitle();
        String author = articlePostRequest.getAuthor();
        String articleContent = articlePostRequest.getArticleContent();
        LocalDate publishDate = articlePostRequest.getPublishDate();
        String starCount = articlePostRequest.getStarCount();

        if (title == null && author == null && articleContent == null
                && publishDate == null && starCount == null) {
            throw new AllParametersAreNullException("Couldn't add a new article record since no parameter in provided!");
        }
        articleRepository.postArticleByParameters(
                title, author, articleContent, publishDate, starCount);
        List<Article> articleList = articleRepository.getArticleListByParameters(
                title, author, articleContent, publishDate, starCount);
        return new ArticlePostResponse(articleList);
    }

    public ArticlePutResponse putArticleByRequest(ArticlePutRequest articlePutRequest) {
        int id = Integer.parseInt(articlePutRequest.getArticleId());

        List<Article> existingArticleList = articleRepository.getArticleListById(id);

        if (existingArticleList.isEmpty()) {
            throw new NotFoundException("Article with id=" + id + " is not found!");
        } else {
            String title = articlePutRequest.getTitle();
            String author = articlePutRequest.getAuthor();
            String articleContent = articlePutRequest.getArticleContent();
            LocalDate publishDate = articlePutRequest.getPublishDate();
            String starCount = articlePutRequest.getStarCount();
            if (title == null && author == null && articleContent == null
                    && publishDate == null && starCount == null) {
                throw new AllParametersAreNullException("Couldn't update the article record since no parameter in provided!");
            } else {
                articleRepository.updateArticleById(
                        id, title, author, articleContent, publishDate, starCount);
                List<Article> newlyUpdatedArticleList = articleRepository.getArticleListById(id);
                return new ArticlePutResponse(newlyUpdatedArticleList);
            }
        }
    }
}
