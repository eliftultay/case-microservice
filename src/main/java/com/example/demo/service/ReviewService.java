package com.example.demo.service;

import com.example.demo.entity.Article;
import com.example.demo.entity.Review;
import com.example.demo.exception.AllParametersAreNullException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.request.review.ReviewDeleteRequest;
import com.example.demo.model.request.review.ReviewGetRequest;
import com.example.demo.model.request.review.ReviewPostRequest;
import com.example.demo.model.request.review.ReviewPutRequest;
import com.example.demo.model.response.review.ReviewDeleteResponse;
import com.example.demo.model.response.review.ReviewGetResponse;
import com.example.demo.model.response.review.ReviewPostResponse;
import com.example.demo.model.response.review.ReviewPutResponse;
import com.example.demo.repository.ArticleRepository;
import com.example.demo.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ArticleRepository articleRepository;


    public ReviewGetResponse getReviewByRequest(ReviewGetRequest reviewGetRequest) {
        List<Review> reviewList;

        if (reviewGetRequest.getReviewId() != null) {
            int id = Integer.parseInt(reviewGetRequest.getReviewId());
            reviewList = reviewRepository.getReviewListById(id);
        } else {
            String articleId = reviewGetRequest.getReviewId();
            String reviewer = reviewGetRequest.getReviewer();
            String reviewContent = reviewGetRequest.getReviewContent();
            reviewList = reviewRepository.getReviewListByParameters(
                    articleId, reviewer, reviewContent);
        }
        return new ReviewGetResponse(reviewList);
    }

    public ReviewDeleteResponse deleteReviewByRequest(ReviewDeleteRequest reviewDeleteRequest) {
        List<Review> reviewList;

        if (reviewDeleteRequest.getReviewId() != null) {
            int id = Integer.parseInt(reviewDeleteRequest.getReviewId());
            reviewList = reviewRepository.getReviewListById(id);
            reviewRepository.deleteReviewById(id);
        } else {
            String articleId = reviewDeleteRequest.getArticleId();
            String reviewer = reviewDeleteRequest.getReviewer();
            String reviewContent = reviewDeleteRequest.getReviewContent();
            if (articleId == null && reviewer == null && reviewContent == null) {
                throw new AllParametersAreNullException("Couldn't delete the review record since no parameter in provided!");
            } else {
                reviewList = reviewRepository.getReviewListByParameters(articleId, reviewer,
                        reviewContent);
                reviewRepository.deleteReviewListByParameters(articleId, reviewer,
                        reviewContent);
            }
        }

        return new ReviewDeleteResponse(reviewList);
    }

    public ReviewPostResponse postReviewByRequest(ReviewPostRequest reviewPostRequest) {
        String articleId = reviewPostRequest.getArticleId();
        String reviewer = reviewPostRequest.getReviewer();
        String reviewContent = reviewPostRequest.getReviewContent();

        if (articleId == null && reviewer == null && reviewContent == null) {
            throw new AllParametersAreNullException("Couldn't add a new review record since no parameter in provided!");
        }

        List<Article> articleList = articleRepository.getArticleListById(Integer.parseInt(articleId));

        reviewRepository.postReviewByParameters(articleList.get(0), reviewer,
                reviewContent);

        List<Review> reviewList = reviewRepository.getReviewListByParameters(
                articleId, reviewer, reviewContent);

        return new ReviewPostResponse(reviewList);
    }

    public ReviewPutResponse putReviewByRequest(ReviewPutRequest reviewPutRequest) {
        int reviewId = Integer.parseInt(reviewPutRequest.getReviewId());

        List<Review> existingReviewList = reviewRepository.getReviewListById(reviewId);

        if (existingReviewList.isEmpty()) {
            throw new NotFoundException("Review with id=" + reviewId + " is not found!");
        } else {
            String articleId = reviewPutRequest.getArticleId();
            String reviewer = reviewPutRequest.getReviewer();
            String reviewContent = reviewPutRequest.getReviewContent();
            if (articleId == null && reviewer == null && reviewContent == null) {
                return new ReviewPutResponse(existingReviewList);
            } else {
                Review review = existingReviewList.get(0);
                List<Article> articleList = articleRepository.getArticleListById(review.getArticle().getId());
                int id = Integer.parseInt(reviewPutRequest.getReviewId());
                reviewRepository.updateReviewById(id, articleList.get(0), reviewer, reviewContent);
                List<Review> newlyUpdatedReviewList = reviewRepository.getReviewListById(id);
                return new ReviewPutResponse(newlyUpdatedReviewList);
            }
        }
    }

}
