package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/*
* Entity is a DB Table, represents DB.
* Convert DB entities into understandable java classes.
* */

@Entity
@Table
@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column
    String title;

    @Column
    String author;

    @Column(name = "article_content")
    String articleContent;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "publish_date")
    LocalDate publishDate;

    @Column(name = "star_count")
    int starCount;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "article")
    List<Review> reviews;
}
