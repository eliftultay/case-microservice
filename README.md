To run the application follow the instructions below. (Java 16 and Mysql 8 used for the prroject.) 

1. Run this command in terminal:
    docker container run --name mysqldb --network case-mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=caseproject -d mysql:8

2. Paste these lines into Dockerfile located in the project directory (we already did that) :
From openjdk:16
    copy ./target/employee-jdbc-0.0.1-SNAPSHOT.jar employee-jdbc-0.0.1-SNAPSHOT.jar
    CMD ["java","-jar","employee-jdbc-0.0.1-SNAPSHOT.jar"]

3. Finish maven install succesfully

4. Navigate into project directory and run this command in terminal:
    docker image build -t case .

5. Run this command in terminal:
    docker container run --network case-mysql --name case-container -p 8080:8080 -d case

6. Finally we are ready to access the endpoints :D
Example
Request:
    http://127.0.0.1:8080/articles?articleId=1
Result:
{"articleList":[{"id":1,"title":"title1","author":"author1","articleContent":"SampleArticleContent1","publishDate":"2001-01-11","starCount":4,"reviews":[{"id":1,"reviewer":"reviewer1","reviewContent":"SampleReviewContent1"}]}]}

